# Commerce sync
If you are looking for a way of migrating your promotions, products, variations and store and deploying them to another environment without manual steps afterward - this module is for you.

## Dependency
This module depends on :
- [Single Content Sync](https://www.drupal.org/project/single_content_sync)
- [Commerce](https://www.drupal.org/project/commerce)

## Installation
Classic installation as other modules. [See the documentation](https://www.drupal.org/docs/extending-drupal/installing-modules)

## Maintainers
* [Ihor Mashevskyi](https://www.drupal.org/u/igor-mashevskyi)
* [Mickaël Silvert](https://www.drupal.org/u/yepa)

## Sponsor
[Happy Coding](https://www.happycoding.ch)
