<?php

namespace Drupal\Tests\commerce_sync\Functional;

use Drupal\user\UserInterface;

/**
 * Test different import functionalities.
 *
 * @group commerce_sync
 */
class CommerceSyncPromotionImportTest extends CommerceSyncBrowserTestBase {

  use CommerceSyncImportContentTrait;

  /**
   * {@inheritdoc}
   */
  protected $profile = 'minimal';

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stable9';

  /**
   * A user with permissions to view and create content.
   *
   * @var \Drupal\user\UserInterface
   */
  protected UserInterface $adminUser;

  /**
   * Test import of a very simple basic page.
   */
  public function testPromotionImport() {
    // Login as admin.
    $this->drupalLogin($this->adminUser);

    // Import a very simple basic page.
    $this->importFile('commerce_promotion');

    // Check if the promotion is created.
    $this->drupalGet('admin/commerce/promotions');
    $this->assertSession()->pageTextContains('Test');

    // Check the store data.
    $this->drupalGet('store/1/edit');
    $this->assertSession()->fieldValueEquals('name[0][value]', 'store name');
    $this->assertSession()->fieldValueEquals('mail[0][value]', 'example@example.com');

    // Check the product data.
    $this->drupalGet('product/1/edit');
    $this->assertSession()->pageTextContains('owner');
    $this->assertSession()->fieldValueEquals('title[0][value]', 'Test product');

    // Check the product variation data.
    $this->drupalGet('product/1/variations');
    $this->assertSession()->pageTextContains('3-20-1693485216a');
    $this->assertSession()->pageTextContains('$35');

    // Check if the owner exist.
    $this->drupalGet('admin/people');
    $this->assertSession()->pageTextContains('owner');

    // Check the promotion data.
    $this->drupalGet('promotion/1/edit');
    $this->assertSession()->fieldValueEquals('name[0][value]', 'Test');
    $this->assertSession()->fieldValueEquals('display_name[0][value]', 'Test display');
    $this->assertSession()->fieldValueEquals('description[0][value]', 'Test description');
    $this->assertSession()->fieldValueEquals('offer[0][target_plugin_configuration][order_item_fixed_amount_off][amount][number]', '50.00');
    $this->assertSession()->fieldValueEquals('offer[0][target_plugin_configuration][order_item_fixed_amount_off][conditions][products][order_item_product][configuration][form][products]', 'Test product (1)');
    $this->assertSession()->fieldValueEquals('offer[0][target_plugin_configuration][order_item_fixed_amount_off][conditions][products][order_item_purchased_entity:commerce_product_variation][configuration][form][entities]', 'Test product (1)');
    $this->assertSession()->fieldValueEquals('start_date[0][value][date]', '2023-10-23');
    $this->assertSession()->fieldValueEquals('start_date[0][value][time]', '19:26:18');

  }

}
