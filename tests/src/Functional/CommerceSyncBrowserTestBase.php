<?php

namespace Drupal\Tests\commerce_sync\Functional;

use Drupal\Tests\BrowserTestBase;
use Drupal\user\UserInterface;

/**
 * Browser test base class for Single content sync functional tests.
 *
 * @group single_content_sync
 */
abstract class CommerceSyncBrowserTestBase extends BrowserTestBase {

  use CommerceSyncImportContentTrait;

  /**
   * {@inheritdoc}
   */
  protected $profile = 'minimal';

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stable9';

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'single_content_sync',
    'commerce_sync',
    'commerce',
    'commerce_price',
    'commerce_product',
    'commerce_promotion',
    'commerce_store',
  ];

  /**
   * A user with permissions to view and create content.
   *
   * @var \Drupal\user\UserInterface
   */
  protected UserInterface $adminUser;

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    $this->adminUser = $this->drupalCreateUser([
      'access administration pages',
      'access commerce administration pages',
      'access commerce_store overview',
      'create online commerce_store',
      'delete any online commerce_store',
      'update any online commerce_store',
      'view commerce_store',
      'access commerce_promotion overview',
      'create commerce_promotion',
      'delete any commerce_promotion',
      'update any commerce_promotion',
      'view commerce_promotion',
      'access commerce_product overview',
      'create default commerce_product',
      'update any default commerce_product',
      'delete any default commerce_product',
      'view commerce_product',
      'manage default commerce_product_variation',
      'import single content',
      'export single content',
      'export commerce_product content',
      'administer users',
    ]);
  }

}
