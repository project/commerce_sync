<?php

namespace Drupal\commerce_sync;

use Drupal\Component\Plugin\PluginManagerInterface;

/**
 * CommerceSyncCondition plugin manager interface.
 */
interface CommerceSyncConditionPluginManagerInterface extends PluginManagerInterface {

  /**
   * Gets the promotion condition plugin for a given plugin id.
   *
   * @param string $conditionPluginId
   *   The condition plugin id.
   *
   * @return \Drupal\commerce_sync\CommerceSyncConditionInterface|null
   *   The condition plugin or NULL if none found.
   *
   * @throws \Drupal\Component\Plugin\Exception\PluginException
   *   If the instance cannot be created, such as if the ID is invalid.
   */
  public function getConditionPluginInstance($conditionPluginId): ?CommerceSyncConditionInterface;

}
