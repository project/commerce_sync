<?php

namespace Drupal\commerce_sync;

use Drupal\Component\Plugin\PluginBase;

/**
 * Base class for commerce_sync_condition plugins.
 */
abstract class CommerceSyncConditionBase extends PluginBase implements CommerceSyncConditionInterface {

  /**
   * {@inheritdoc}
   */
  public function label(): string {
    // Cast the label to a string since it is a TranslatableMarkup object.
    return (string) $this->pluginDefinition['label'];
  }

}
