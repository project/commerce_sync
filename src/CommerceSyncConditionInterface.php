<?php

namespace Drupal\commerce_sync;

/**
 * Interface for commerce_sync_condition plugins.
 */
interface CommerceSyncConditionInterface {

  /**
   * Returns the translated plugin label.
   *
   * @return string
   *   The translated title.
   */
  public function label(): string;

  /**
   * Exports the promotion condition with references to a serializable format.
   *
   * @param array $condition
   *   The entity to export.
   *
   * @return array
   *   The promotion condition with references.
   */
  public function exportConditionReference(array $condition): array;

  /**
   * Do a mapping between promotion condition and exported content.
   *
   * @param array $values
   *   Original exported values of promotion condition with references.
   *
   * @return array
   *   Correct promotion condition mapping with exported values.
   */
  public function mapConditionsReference(array $values): array;

}
