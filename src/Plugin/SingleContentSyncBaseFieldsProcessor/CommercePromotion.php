<?php

namespace Drupal\commerce_sync\Plugin\SingleContentSyncBaseFieldsProcessor;

use Drupal\commerce\Plugin\Commerce\Condition\ConditionBase;
use Drupal\commerce_promotion\Entity\PromotionInterface;
use Drupal\commerce_promotion\Plugin\Commerce\PromotionOffer\PromotionOfferInterface;
use Drupal\commerce_sync\CommerceSyncConditionInterface;
use Drupal\commerce_sync\CommerceSyncConditionPluginManagerInterface;
use Drupal\Core\Entity\EntityRepositoryInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Entity\FieldableEntityInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\datetime\Plugin\Field\FieldType\DateTimeItemInterface;
use Drupal\single_content_sync\ContentExporterInterface;
use Drupal\single_content_sync\ContentImporterInterface;
use Drupal\single_content_sync\SingleContentSyncBaseFieldsProcessorPluginBase;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Plugin implementation for commerce product base fields processor plugin.
 *
 * @SingleContentSyncBaseFieldsProcessor(
 *   id = "commerce_promotion",
 *   label = @Translation("Commerce Promotion"),
 *   entity_type = "commerce_promotion",
 * )
 */
class CommercePromotion extends SingleContentSyncBaseFieldsProcessorPluginBase implements ContainerFactoryPluginInterface {

  /**
   * The content exporter.
   *
   * @var \Drupal\single_content_sync\ContentExporterInterface
   */
  protected ContentExporterInterface $exporter;

  /**
   * The entity repository.
   *
   * @var \Drupal\Core\Entity\EntityRepositoryInterface
   */
  protected EntityRepositoryInterface $entityRepository;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected EntityTypeManagerInterface $entityTypeManager;

  /**
   * The content importer.
   *
   * @var \Drupal\single_content_sync\ContentImporterInterface
   */
  protected ContentImporterInterface $importer;

  /**
   * The commerce sync condition plugin manager..
   *
   * @var \Drupal\commerce_sync\CommerceSyncConditionPluginManagerInterface
   */
  protected CommerceSyncConditionPluginManagerInterface $commerceSyncConditionManager;

  /**
   * {@inheritdoc}
   */
  public function __construct(
    array $configuration,
    $plugin_id,
    $plugin_definition,
    ContentExporterInterface $exporter,
    EntityRepositoryInterface $entity_repository,
    EntityTypeManagerInterface $entity_type_manager,
    ContentImporterInterface $importer,
    CommerceSyncConditionPluginManagerInterface $commerce_sync_condition_manager
  ) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);

    $this->exporter = $exporter;
    $this->entityRepository = $entity_repository;
    $this->entityTypeManager = $entity_type_manager;
    $this->importer = $importer;
    $this->commerceSyncConditionManager = $commerce_sync_condition_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('single_content_sync.exporter'),
      $container->get('entity.repository'),
      $container->get('entity_type.manager'),
      $container->get('single_content_sync.importer'),
      $container->get('plugin.manager.commerce_sync_condition')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function exportBaseValues(FieldableEntityInterface $entity): array {
    assert($entity instanceof PromotionInterface);

    $base_fields = [
      'name' => $entity->label(),
      'display_name' => $entity->getDisplayName(),
      'description' => $entity->getDescription(),
      'langcode' => $entity->language()->getId(),
      'uid' => $entity->getOwnerId()
        ? $this->exporter->doExportToArray($entity->getOwner())
        : 0,
      'condition_operator' => $entity->getConditionOperator(),
      'usage_limit' => $entity->getUsageLimit(),
      'usage_limit_customer' => $entity->getCustomerUsageLimit(),
      'start_date' => $entity->get('start_date')->isEmpty()
        ? NULL
        : $entity->getStartDate()->format(DateTimeItemInterface::DATETIME_STORAGE_FORMAT),
      'end_date' => $entity->get('end_date')->isEmpty()
        ? NULL
        : $entity->getEndDate()->format(DateTimeItemInterface::DATETIME_STORAGE_FORMAT),
      'compatibility' => $entity->getCompatibility(),
      'require_coupon' => $entity->requiresCoupon(),
      'status' => $entity->isEnabled(),
      'weight' => $entity->getWeight(),
      'default_langcode' => $entity->isDefaultTranslation(),
      'offer' => [],
      'conditions' => [],
      'stores' => empty($entity->getStoreIds())
        ? []
        : array_map(function ($store) {
          return $this->exporter->doExportToArray($store);
        }, $entity->getStores()),
    ];
    /** @var \Drupal\commerce_promotion\Plugin\Commerce\PromotionOffer\OrderItemPromotionOfferInterface $offer */
    if ($offer = $entity->getOffer()) {
      $base_fields['offer'] = $this->exportOffer($offer);
    }

    foreach ($entity->getConditions() as $key => $condition) {
      $base_fields['conditions'][$key] = $this->exportConditionPlugin($condition);
    }

    return $base_fields;
  }

  /**
   * {@inheritdoc}
   */
  public function mapBaseFieldsValues(array $values, FieldableEntityInterface $entity): array {
    $base_fields = [
      'name' => $values['name'],
      'display_name' => $values['display_name'],
      'description' => $values['description'],
      'langcode' => $values['langcode'],
      'condition_operator' => $values['condition_operator'],
      'usage_limit' => $values['usage_limit'],
      'usage_limit_customer' => $values['usage_limit_customer'],
      'start_date' => $values['start_date'],
      'end_date' => $values['end_date'],
      'compatibility' => $values['compatibility'],
      'require_coupon' => $values['require_coupon'],
      'status' => $values['status'],
      'weight' => $values['weight'],
      'default_langcode' => $values['default_langcode'],
    ];

    // Import user entity.
    if (!empty($values['uid'])) {
      $user = $this->importer->doImport($values['uid']);
      $base_fields['uid'] = $user->id();
    }

    // Import stores entity.
    if (!empty($values['stores'])) {
      foreach ($values['stores'] as $sid) {
        $store = $this->importer->doImport($sid);
        $base_fields['stores'][] = $store->id();
      }
    }

    // Import offer.
    if (!empty($values['offer'])) {
      if (!empty($values['offer']['target_plugin_configuration']['conditions'])) {
        foreach ($values['offer']['target_plugin_configuration']['conditions'] as &$offer_condition) {
          if (empty($offer_condition['plugin']) || empty($offer_condition['configuration'])) {
            continue;
          }

          $offer_condition['configuration'] = $this->importCondition($offer_condition['plugin'], $offer_condition['configuration']);
        }
      }
      $base_fields['offer'] = $values['offer'];
    }

    // Import conditions.
    if (!empty($values['conditions'])) {
      foreach ($values['conditions'] as &$condition) {
        if (empty($condition['target_plugin_id']) || empty($condition['target_plugin_configuration'])) {
          continue;
        }

        $condition['target_plugin_configuration'] = $this->importCondition($condition['target_plugin_id'], $condition['target_plugin_configuration']);
      }

      $base_fields['conditions'] = $values['conditions'];
    }

    return $base_fields;
  }

  /**
   * Export offer.
   *
   * @param \Drupal\commerce_promotion\Plugin\Commerce\PromotionOffer\PromotionOfferInterface $offer
   *   The offer.
   *
   * @return array
   *   Offer data with dependencies.
   */
  protected function exportOffer(PromotionOfferInterface $offer): array {
    $plugin_id = $offer->getPluginId();
    $configuration = $offer->getConfiguration();
    if (!empty($configuration['conditions'])) {
      foreach ($configuration['conditions'] as $key => $condition) {
        $configuration['conditions'][$key] = [
          'plugin' => $condition['plugin'],
          'configuration' => $this->exportCondition($condition['plugin'], $condition['configuration']),
        ];
      }
    }

    return [
      'target_plugin_id' => $plugin_id,
      'target_plugin_configuration' => $configuration,
    ];
  }

  /**
   * Import condition.
   *
   * @param string $plugin_id
   *   Plugin id.
   * @param array $configuration
   *   Condition configuration.
   *
   * @return array
   *   Condition configuration after importing dependencies.
   */
  protected function importCondition($plugin_id, array $configuration): array {
    $plugin = $this->commerceSyncConditionManager->getConditionPluginInstance($plugin_id);

    if ($plugin instanceof CommerceSyncConditionInterface) {
      $configuration = $plugin->mapConditionsReference($configuration);
    }

    return $configuration;
  }

  /**
   * Export condition from condition plugin.
   *
   * @param \Drupal\commerce\Plugin\Commerce\Condition\ConditionBase $condition
   *   The offer.
   *
   * @return array
   *   Condition data with dependencies.
   */
  protected function exportConditionPlugin(ConditionBase $condition): array {
    $plugin_id = $condition->getPluginId();
    $configuration = $condition->getConfiguration();

    if (!empty($configuration)) {
      $configuration = $this->exportCondition($plugin_id, $configuration);
    }

    return [
      'target_plugin_id' => $plugin_id,
      'target_plugin_configuration' => $configuration,
    ];
  }

  /**
   * Export condition.
   *
   * @param string $plugin_id
   *   Plugin id.
   * @param array $configuration
   *   Condition configuration.
   *
   * @return array
   *   Condition data with dependencies.
   */
  protected function exportCondition($plugin_id, array $configuration): array {
    $plugin = $this->commerceSyncConditionManager->getConditionPluginInstance($plugin_id);

    if ($plugin instanceof CommerceSyncConditionInterface) {
      $configuration = $plugin->exportConditionReference($configuration);
    }

    return $configuration;
  }

}
