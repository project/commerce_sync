<?php

namespace Drupal\commerce_sync\Plugin\SingleContentSyncBaseFieldsProcessor;

use Drupal\commerce_product\Entity\ProductVariationInterface;
use Drupal\Core\Entity\EntityRepositoryInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Entity\FieldableEntityInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\single_content_sync\ContentExporterInterface;
use Drupal\single_content_sync\ContentImporterInterface;
use Drupal\single_content_sync\SingleContentSyncBaseFieldsProcessorPluginBase;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Implementation for commerce product variation base fields processor plugin.
 *
 * @SingleContentSyncBaseFieldsProcessor(
 *   id = "commerce_product_variation",
 *   label = @Translation("Commerce Product Variation"),
 *   entity_type = "commerce_product_variation",
 * )
 */
class CommerceProductVariation extends SingleContentSyncBaseFieldsProcessorPluginBase implements ContainerFactoryPluginInterface {

  /**
   * The content exporter.
   *
   * @var \Drupal\single_content_sync\ContentExporterInterface
   */
  protected ContentExporterInterface $exporter;

  /**
   * The entity repository.
   *
   * @var \Drupal\Core\Entity\EntityRepositoryInterface
   */
  protected EntityRepositoryInterface $entityRepository;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected EntityTypeManagerInterface $entityTypeManager;

  /**
   * The content importer.
   *
   * @var \Drupal\single_content_sync\ContentImporterInterface
   */
  protected ContentImporterInterface $importer;

  /**
   * {@inheritdoc}
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, ContentExporterInterface $exporter, EntityRepositoryInterface $entity_repository, EntityTypeManagerInterface $entity_type_manager, ContentImporterInterface $importer) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);

    $this->exporter = $exporter;
    $this->entityRepository = $entity_repository;
    $this->entityTypeManager = $entity_type_manager;
    $this->importer = $importer;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('single_content_sync.exporter'),
      $container->get('entity.repository'),
      $container->get('entity_type.manager'),
      $container->get('single_content_sync.importer')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function exportBaseValues(FieldableEntityInterface $entity): array {
    assert($entity instanceof ProductVariationInterface);

    $base_fields = [
      'title' => $entity->getTitle(),
      'type' => $entity->bundle(),
      'status' => $entity->isPublished(),
      'langcode' => $entity->language()->getId(),
      'default_langcode' => $entity->isDefaultTranslation(),
      'price' => [],
      'list_price' => [],
      'sku' => $entity->getSku(),
      'uid' => $entity->getOwnerId()
        ? $this->exporter->doExportToArray($entity->getOwner())
        : 0,
      // @todo Export product Currently export only uuid to avoid recursion.
      'product_id' => $entity->getProductId()
        ? $entity->getProduct()->uuid()
        : 0,
    ];

    // Add price.
    if ($price = $entity->getPrice()) {
      $base_fields['price'] = $price->toArray();
    }

    // Add list price.
    if ($list_price = $entity->getListPrice()) {
      $base_fields['list_price'] = $list_price->toArray();
    }

    return $base_fields;
  }

  /**
   * {@inheritdoc}
   */
  public function mapBaseFieldsValues(array $values, FieldableEntityInterface $entity): array {
    $base_fields = [
      'title' => $values['title'],
      'type' => $values['type'],
      'status' => $values['status'],
      'langcode' => $values['langcode'],
      'default_langcode' => $values['default_langcode'],
      'sku' => $values['sku'],
      'price' => $values['price'],
      'list_price' => $values['list_price'],
    ];

    // Import user entity.
    if (!empty($values['uid'])) {
      $user = $this->importer->doImport($values['uid']);
      $base_fields['uid'] = $user->id();
    }

    // Import product entity.
    if ($product_id = $values['product_id'] ?? NULL) {
      $product = $this->entityRepository->loadEntityByUuid('commerce_product', $product_id);
      if ($product) {
        $base_fields['product_id'] = $product->id();
      }
    }

    return $base_fields;
  }

}
