<?php

namespace Drupal\commerce_sync\Plugin\SingleContentSyncBaseFieldsProcessor;

use Drupal\commerce_product\Entity\ProductInterface;
use Drupal\Core\Entity\EntityRepositoryInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Entity\FieldableEntityInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\single_content_sync\ContentExporterInterface;
use Drupal\single_content_sync\ContentImporterInterface;
use Drupal\single_content_sync\SingleContentSyncBaseFieldsProcessorPluginBase;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Plugin implementation for commerce product base fields processor plugin.
 *
 * @SingleContentSyncBaseFieldsProcessor(
 *   id = "commerce_product",
 *   label = @Translation("Commerce Product"),
 *   entity_type = "commerce_product",
 * )
 */
class CommerceProduct extends SingleContentSyncBaseFieldsProcessorPluginBase implements ContainerFactoryPluginInterface {

  /**
   * The content exporter.
   *
   * @var \Drupal\single_content_sync\ContentExporterInterface
   */
  protected ContentExporterInterface $exporter;

  /**
   * The entity repository.
   *
   * @var \Drupal\Core\Entity\EntityRepositoryInterface
   */
  protected EntityRepositoryInterface $entityRepository;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected EntityTypeManagerInterface $entityTypeManager;

  /**
   * The content importer.
   *
   * @var \Drupal\single_content_sync\ContentImporterInterface
   */
  protected ContentImporterInterface $importer;

  /**
   * {@inheritdoc}
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, ContentExporterInterface $exporter, EntityRepositoryInterface $entity_repository, EntityTypeManagerInterface $entity_type_manager, ContentImporterInterface $importer) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);

    $this->exporter = $exporter;
    $this->entityRepository = $entity_repository;
    $this->entityTypeManager = $entity_type_manager;
    $this->importer = $importer;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('single_content_sync.exporter'),
      $container->get('entity.repository'),
      $container->get('entity_type.manager'),
      $container->get('single_content_sync.importer')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function exportBaseValues(FieldableEntityInterface $entity): array {
    assert($entity instanceof ProductInterface);

    $base_fields = [
      'title' => $entity->label(),
      'type' => $entity->bundle(),
      'status' => $entity->isPublished(),
      'langcode' => $entity->language()->getId(),
      'default_langcode' => $entity->isDefaultTranslation(),
      'uid' => $entity->getOwnerId()
        ? $this->exporter->doExportToArray($entity->getOwner())
        : 0,
      'variations' => empty($entity->getVariationIds())
        ? []
        : array_map(function ($variation) {
          return $this->exporter->doExportToArray($variation);
        }, $entity->getVariations()),
      'stores' => empty($entity->getStoreIds())
        ? []
        : array_map(function ($store) {
          return $this->exporter->doExportToArray($store);
        }, $entity->getStores()),
    ];

    return $base_fields;
  }

  /**
   * {@inheritdoc}
   */
  public function mapBaseFieldsValues(array $values, FieldableEntityInterface $entity): array {
    $base_fields = [
      'title' => $values['title'],
      'type' => $values['type'],
      'status' => $values['status'],
      'langcode' => $values['langcode'],
      'default_langcode' => $values['default_langcode'],
    ];

    // Import user entity.
    if (!empty($values['uid'])) {
      $user = $this->importer->doImport($values['uid']);
      $base_fields['uid'] = $user->id();
    }

    // Import variations entity.
    if (!empty($values['variations'])) {
      foreach ($values['variations'] as $vid) {
        $commerce_product_variation = $this->importer->doImport($vid);
        $base_fields['variations'][] = $commerce_product_variation->id();
      }
    }

    // Import stores entity.
    if (!empty($values['stores'])) {
      foreach ($values['stores'] as $sid) {
        $store = $this->importer->doImport($sid);
        $base_fields['stores'][] = $store->id();
      }
    }

    return $base_fields;
  }

}
