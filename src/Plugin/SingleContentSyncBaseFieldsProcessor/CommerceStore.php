<?php

namespace Drupal\commerce_sync\Plugin\SingleContentSyncBaseFieldsProcessor;

use Drupal\commerce_store\Entity\StoreInterface;
use Drupal\Core\Entity\FieldableEntityInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\single_content_sync\ContentExporterInterface;
use Drupal\single_content_sync\ContentImporterInterface;
use Drupal\single_content_sync\SingleContentSyncBaseFieldsProcessorPluginBase;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Plugin implementation for commerce product base fields processor plugin.
 *
 * @SingleContentSyncBaseFieldsProcessor(
 *   id = "commerce_store",
 *   label = @Translation("Commerce Store"),
 *   entity_type = "commerce_store",
 * )
 */
class CommerceStore extends SingleContentSyncBaseFieldsProcessorPluginBase implements ContainerFactoryPluginInterface {

  /**
   * The content exporter.
   *
   * @var \Drupal\single_content_sync\ContentExporterInterface
   */
  protected ContentExporterInterface $exporter;

  /**
   * The content importer.
   *
   * @var \Drupal\single_content_sync\ContentImporterInterface
   */
  protected ContentImporterInterface $importer;

  /**
   * {@inheritdoc}
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, ContentExporterInterface $exporter, ContentImporterInterface $importer) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->exporter = $exporter;
    $this->importer = $importer;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('single_content_sync.exporter'),
      $container->get('single_content_sync.importer')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function exportBaseValues(FieldableEntityInterface $entity): array {
    assert($entity instanceof StoreInterface);

    return [
      'name' => $entity->label(),
      'mail' => $entity->getEmail(),
      'type' => $entity->bundle(),
      'langcode' => $entity->language()->getId(),
      'is_default' => $entity->isDefault(),
      'default_langcode' => $entity->isDefaultTranslation(),
      'default_currency' => $entity->getDefaultCurrencyCode(),
      'timezone' => $entity->getTimezone(),
      'address' => $entity->getAddress() ? $entity->getAddress()->getValue() : [],
      'uid' => $entity->getOwnerId()
        ? $this->exporter->doExportToArray($entity->getOwner())
        : 0,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function mapBaseFieldsValues(array $values, FieldableEntityInterface $entity): array {
    $base_fields = [
      'name' => $values['name'],
      'mail' => $values['mail'],
      'type' => $values['type'],
      'langcode' => $values['langcode'],
      'is_default' => $values['is_default'],
      'default_langcode' => $values['default_langcode'],
      'default_currency' => $values['default_currency'],
      'timezone' => $values['timezone'],
      'address' => $values['address'],
    ];

    // Import user entity.
    if (!empty($values['uid'])) {
      $user = $this->importer->doImport($values['uid']);
      $base_fields['uid'] = $user->id();
    }

    return $base_fields;
  }

}
