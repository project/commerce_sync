<?php

namespace Drupal\commerce_sync\Plugin\CommerceSyncCondition;

use Drupal\commerce\PurchasableEntityInterface;
use Drupal\Component\Plugin\Derivative\DeriverBase;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Plugin\Discovery\ContainerDeriverInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Deriver for purchased entity condition plugins.
 */
class PurchasedEntityConditionDeriver extends DeriverBase implements ContainerDeriverInterface {

  /**
   * Constructs a new PurchasedEntityConditionDeriver object.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   The entity type manager.
   */
  public function __construct(protected EntityTypeManagerInterface $entityTypeManager) {}

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, $base_plugin_id) {
    return new static(
      $container->get('entity_type.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getDerivativeDefinitions($base_plugin_definition) {
    $purchasable_entity_types = array_filter($this->entityTypeManager->getDefinitions(), static function (EntityTypeInterface $entity_type) {
      return $entity_type->entityClassImplements(PurchasableEntityInterface::class);
    });

    // Get entity type id form plugin id - trim "_purchased_entity".
    $entity_type = substr($base_plugin_definition['id'] ?? '', 0, -17);

    foreach ($purchasable_entity_types as $purchasable_entity_type_id => $purchasable_entity_type) {
      $this->derivatives[$purchasable_entity_type_id] = [
        'label' => $purchasable_entity_type->getLabel(),
        'id' => "{$entity_type}_purchased_entity:$purchasable_entity_type_id",
        'condition_plugin_id' => "{$entity_type}_purchased_entity:$purchasable_entity_type_id",
      ] + $base_plugin_definition;
    }

    return parent::getDerivativeDefinitions($base_plugin_definition);
  }

}
