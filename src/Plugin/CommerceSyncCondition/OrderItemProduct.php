<?php

namespace Drupal\commerce_sync\Plugin\CommerceSyncCondition;

/**
 * Plugin implementation for commerce product promotion condition plugin.
 *
 * @CommerceSyncCondition(
 *   id = "order_item_product",
 *   label = @Translation("Commerce product"),
 *   condition_plugin_id = "order_item_product"
 * )
 */
class OrderItemProduct extends ProductBase {}
