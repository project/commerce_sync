<?php

namespace Drupal\commerce_sync\Plugin\CommerceSyncCondition;

/**
 * Plugin implementation for commerce product promotion condition plugin.
 *
 * @CommerceSyncCondition(
 *   id = "order_purchased_entity",
 *   deriver = "Drupal\commerce_sync\Plugin\CommerceSyncCondition\PurchasedEntityConditionDeriver"
 * )
 */
class OrderPurchasedEntity extends PurchasedEntityBase {}
