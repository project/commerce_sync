<?php

namespace Drupal\commerce_sync\Plugin\CommerceSyncCondition;

use Drupal\commerce_sync\CommerceSyncConditionBase;
use Drupal\Core\Entity\EntityRepositoryInterface;
use Drupal\Core\Entity\FieldableEntityInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\single_content_sync\ContentExporterInterface;
use Drupal\single_content_sync\ContentImporterInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Base class for commerce product promotion condition plugin.
 */
class ProductBase extends CommerceSyncConditionBase implements ContainerFactoryPluginInterface {

  /**
   * Constructs ProductBase object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Entity\EntityRepositoryInterface $entityRepository
   *   The entity repository.
   * @param \Drupal\single_content_sync\ContentExporterInterface $exporter
   *   The content exporter service.
   * @param \Drupal\single_content_sync\ContentImporterInterface $importer
   *   The content importer service.
   */
  public function __construct(
    array $configuration,
    $plugin_id,
    $plugin_definition,
    protected EntityRepositoryInterface $entityRepository,
    protected ContentExporterInterface $exporter,
    protected ContentImporterInterface $importer
  ) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity.repository'),
      $container->get('single_content_sync.exporter'),
      $container->get('single_content_sync.importer')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function exportConditionReference(array $condition): array {
    if (!empty($condition['products'])) {
      foreach ($condition['products'] as $key => $value) {
        $uuid = $value['product'];
        $product = $this->entityRepository
          ->loadEntityByUuid('commerce_product', $uuid);
        if ($product instanceof FieldableEntityInterface) {
          $condition['products'][$key] = $this->exporter->doExportToArray($product);
        }
      }
    }

    return $condition;
  }

  /**
   * {@inheritdoc}
   */
  public function mapConditionsReference(array $values): array {
    if (!empty($values['products'])) {
      foreach ($values['products'] as $key => $item) {
        if ($product = $this->importer->doImport($item)) {
          $values['products'][$key] = [
            'product' => $product->get('uuid')->value,
          ];
        }
      }
    }

    return $values;
  }

}
