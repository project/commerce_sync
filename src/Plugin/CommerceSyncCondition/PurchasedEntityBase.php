<?php

namespace Drupal\commerce_sync\Plugin\CommerceSyncCondition;

use Drupal\commerce_sync\CommerceSyncConditionBase;
use Drupal\Core\Entity\EntityRepositoryInterface;
use Drupal\Core\Entity\FieldableEntityInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\single_content_sync\ContentExporterInterface;
use Drupal\single_content_sync\ContentImporterInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Plugin implementation for commerce product promotion condition plugin.
 */
class PurchasedEntityBase extends CommerceSyncConditionBase implements ContainerFactoryPluginInterface {

  /**
   * Constructs OrderItemPurchasedEntity object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Entity\EntityRepositoryInterface $entityRepository
   *   The entity repository.
   * @param \Drupal\single_content_sync\ContentExporterInterface $exporter
   *   The content exporter service.
   * @param \Drupal\single_content_sync\ContentImporterInterface $importer
   *   The content importer service.
   */
  public function __construct(
    array $configuration,
    $plugin_id,
    $plugin_definition,
    protected EntityRepositoryInterface $entityRepository,
    protected ContentExporterInterface $exporter,
    protected ContentImporterInterface $importer
  ) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity.repository'),
      $container->get('single_content_sync.exporter'),
      $container->get('single_content_sync.importer')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function exportConditionReference(array $condition): array {
    if (!empty($condition['entities'])) {
      foreach ($condition['entities'] as $key => $uuid) {
        $entity = $this->entityRepository
          ->loadEntityByUuid($this->getDerivativeId(), $uuid);
        if ($entity instanceof FieldableEntityInterface) {
          $condition['entities'][$key] = $this->exporter->doExportToArray($entity);
        }
      }
    }

    return $condition;
  }

  /**
   * {@inheritdoc}
   */
  public function mapConditionsReference(array $values): array {
    if (!empty($values['entities'])) {
      foreach ($values['entities'] as $key => $item) {
        if ($entity = $this->importer->doImport($item)) {
          $values['entities'][$key] = $entity->get('uuid')->value;
        }
      }
    }

    return $values;
  }

}
