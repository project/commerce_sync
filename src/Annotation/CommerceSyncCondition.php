<?php

namespace Drupal\commerce_sync\Annotation;

use Drupal\Component\Annotation\Plugin;

/**
 * Defines commerce_sync_condition annotation object.
 *
 * @Annotation
 */
class CommerceSyncCondition extends Plugin {

  /**
   * The plugin ID.
   *
   * @var string
   */
  public string $id;

  /**
   * The human-readable name of the plugin.
   *
   * @var \Drupal\Core\Annotation\Translation
   *
   * @ingroup plugin_translatable
   */
  public $label;

  /**
   * The condition plugin id.
   *
   * @var string
   */
  public string $condition_plugin_id;

  /**
   * The plugin weight.
   *
   * Allow to override plugins using weight.
   *
   * @var int
   */
  public int $weight = 0;

}
