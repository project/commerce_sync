<?php

namespace Drupal\commerce_sync;

use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Plugin\DefaultPluginManager;

/**
 * CommerceSyncCondition plugin manager.
 */
class CommerceSyncConditionPluginManager extends DefaultPluginManager implements CommerceSyncConditionPluginManagerInterface {

  /**
   * An array of promotion condition plugin definitions, keyed by plugin id.
   *
   * Used for performance optimization to avoid repeated calls to
   * getDefinitions().
   *
   * @var array|null
   */
  protected ?array $conditionPluginPluginDefinitions = NULL;

  /**
   * Constructs CommerceSyncConditionPluginManager.
   *
   * @param \Traversable $namespaces
   *   An object that implements \Traversable which contains the root paths
   *   keyed by the corresponding namespace to look for plugin implementations.
   * @param \Drupal\Core\Cache\CacheBackendInterface $cacheBackend
   *   Cache backend instance to use.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $moduleHandler
   *   The module handler to invoke the alter hook with.
   */
  public function __construct(
    \Traversable $namespaces,
    CacheBackendInterface $cacheBackend,
    ModuleHandlerInterface $moduleHandler
  ) {
    parent::__construct(
      'Plugin/CommerceSyncCondition',
      $namespaces,
      $moduleHandler,
      'Drupal\commerce_sync\CommerceSyncConditionInterface',
      'Drupal\commerce_sync\Annotation\CommerceSyncCondition'
    );
    $this->alterInfo('single_content_sync_condition_info');
    $this->setCacheBackend($cacheBackend, 'single_content_sync_condition_plugins');
  }

  /**
   * {@inheritdoc}
   */
  public function createInstance($plugin_id, array $configuration = []): CommerceSyncConditionInterface {
    $instance = parent::createInstance($plugin_id, $configuration);

    // Throw an exception if the plugin does not implement the interface.
    if (!$instance instanceof CommerceSyncConditionInterface) {
      throw new \InvalidArgumentException(
        sprintf(
          'The plugin %s does not implement the CommerceSyncConditionInterface.',
          $plugin_id
        )
      );
    }

    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function getConditionPluginInstance($conditionPluginId): ?CommerceSyncConditionInterface {
    $pluginDefinition = $this->getEntityTypesPlugins()[$conditionPluginId] ?? NULL;

    return $pluginDefinition
      ? $this->createInstance($pluginDefinition['id'])
      : NULL;
  }

  /**
   * Returns a list of promotion condition reference plugin definitions.
   *
   * @return array
   *   The list of promotion condition reference plugin definitions,
   *   keyed by condition plugin id.
   */
  protected function getEntityTypesPlugins(): array {
    if (!isset($this->conditionPluginPluginDefinitions)) {
      $this->conditionPluginPluginDefinitions = [];
      foreach ($this->getDefinitions() as $definition) {
        if (
          isset($this->conditionPluginPluginDefinitions[$definition['condition_plugin_id']])
          && $this->conditionPluginPluginDefinitions[$definition['condition_plugin_id']]['weight'] > $definition['weight']
        ) {
          continue;
        }
        $this->conditionPluginPluginDefinitions[$definition['condition_plugin_id']] = $definition;
      }
    }

    return $this->conditionPluginPluginDefinitions;
  }

}
